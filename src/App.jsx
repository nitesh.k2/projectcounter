import React from 'react'
import useFetch from 'react-fetch-hook'
import { Heading } from './components/atoms'

import './styles/main.scss'

const App = () => {
  const { isLoading, data: contentData } = useFetch(
    '/public/content/en-us.json'
  )

  if (isLoading) {
    return 'loading...'
  }
  const { heading } = contentData

  return <Heading lavel='h2'>{heading.value}</Heading>
}

export default App
